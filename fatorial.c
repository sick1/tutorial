#include "fatorial.h"

int fatorial(int x){
	if (x > 0) {
        int fat = 1;
        while (x > 0) {
            fat *= x;
            x--;
        }
        return fat;
    } else {
    	
        return -1;
    }
}